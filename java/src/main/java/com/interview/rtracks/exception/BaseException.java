package com.interview.rtracks.exception;

public class BaseException extends Exception {
    private Long code;

    public BaseException(String message, Long code) {
        super(message);
        this.code = code;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }
}
