package com.interview.rtracks.exception;

public class ManagerException extends BaseException{
    public ManagerException(String message, Long code) {
        super(message, code);
    }
}
