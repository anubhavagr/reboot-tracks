package com.interview.rtracks.response;

import java.util.List;

public abstract class BaseResponse<Entry> {
    protected Status status;
    protected List<Entry> entries;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public List<Entry> getEntries() {
        return entries;
    }

    public void setEntries(List<Entry> entries) {
        this.entries = entries;
    }
}
