package com.interview.rtracks.response;

import com.interview.rtracks.entry.PlaylistEntry;

import java.util.List;

public class PlaylistResponse extends BaseResponse {
    public PlaylistResponse() {
    }

    public PlaylistResponse(List<PlaylistEntry> playlists) {
        this.entries = playlists;
    }
}
