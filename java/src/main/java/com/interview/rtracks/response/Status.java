package com.interview.rtracks.response;

import java.io.Serializable;

public class Status implements Serializable {
    private Long code;
    private String message;
    private StatusType type;

    public Status() {
    }

    public Status(Long code, String message, StatusType type) {
        this.code = code;
        this.message = message;
        this.type = type;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public StatusType getType() {
        return type;
    }

    public void setType(StatusType type) {
        this.type = type;
    }
}
