package com.interview.rtracks.response;


import com.interview.rtracks.entry.TagEntry;

import java.util.List;

public class TagResponse extends BaseResponse<TagEntry> {
    public TagResponse() {
    }

    public TagResponse(List<TagEntry> tags) {
        this.entries = tags;
    }
}
