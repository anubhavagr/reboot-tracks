package com.interview.rtracks.response;

import com.interview.rtracks.entry.SearchEntry;

public class SearchResponse {
    private Status status;
    private SearchEntry search;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public SearchEntry getSearch() {
        return search;
    }

    public void setSearch(SearchEntry search) {
        this.search = search;
    }
}
