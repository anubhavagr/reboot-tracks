package com.interview.rtracks.manager.impl;

import com.interview.rtracks.entity.Tag;
import com.interview.rtracks.entry.TagEntry;
import com.interview.rtracks.manager.TagManager;
import com.interview.rtracks.repository.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Component("tagManager")
public class TagManagerImpl extends BaseManagerImpl<Tag, TagEntry> implements TagManager {

    @Autowired
    @Qualifier("tagRepository")
    public void setRepository(TagRepository repository) {
        this.repository = repository;
    }

    protected Tag convertToEntity(TagEntry tagEntry) {
        return convertToEntity(tagEntry, new Tag());
    }

    protected Tag convertToEntity(TagEntry tagEntry, Tag tag) {
        beanMapper.map(tagEntry, tag);
        return tag;
    }

    protected TagEntry convertToEntry(Tag tag) {
        return beanMapper.map(tag, TagEntry.class);
    }
}
