package com.interview.rtracks.manager.impl;

import com.interview.rtracks.entity.BaseNodeEntity;
import com.interview.rtracks.entry.BaseEntry;
import com.interview.rtracks.exception.ManagerException;
import com.interview.rtracks.manager.BaseManager;
import com.interview.rtracks.repository.NodeRepository;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseManagerImpl<Entity extends BaseNodeEntity, Entry extends BaseEntry> implements BaseManager<Entity, Entry> {
    protected Mapper beanMapper;
    protected NodeRepository<Entity> repository;
    protected final Class<Entity> entityClass;
    protected final Class<Entry> entryClass;

    @Autowired
    public void setRepository(NodeRepository<Entity> repository) {
        this.repository = repository;
    }

    @Autowired
    @Qualifier("beanMapper")
    public void setBeanMapper(Mapper beanMapper) {
        this.beanMapper = beanMapper;
    }

    public BaseManagerImpl() {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        this.entityClass = (Class<Entity>) genericSuperclass.getActualTypeArguments()[0];
        this.entryClass = (Class<Entry>) genericSuperclass.getActualTypeArguments()[1];
    }

    public Entry create(Entry entry) throws ManagerException {
        Entity entity = convertToEntity(entry);
        Entity savedEntity = repository.save(entity, 0);
        Entry savedEntry = convertToEntry(savedEntity);
        return savedEntry;
    }

    public Entry update(Long id, Entry entry) throws ManagerException {
        Entity savedEntity = repository.findById(id, 0);
        convertToEntity(entry, savedEntity);
        repository.save(savedEntity, 1);
        Entry savedEntry = convertToEntry(savedEntity);
        return savedEntry;
    }

    public void delete(Long id) throws ManagerException {
        Entity savedEntity = repository.findById(id, 0);
        repository.delete(savedEntity);
    }

    public Entry findById(Long id) throws ManagerException {
        Entity savedEntity = repository.findById(id, 1);
        return convertToEntry(savedEntity);
    }

    public List<Entry> findByTagNames(List<String> tagNames){
        List<Entry> entries = new ArrayList<>();
        List<Entity>  entities = repository.findByTagNames(tagNames);
        for (Entity entity: entities) {
            entries.add(convertToEntry(entity));
        }
        return entries;
    }

    protected abstract Entity convertToEntity(Entry entry);

    protected abstract Entity convertToEntity(Entry entry, Entity entity);

    protected abstract Entry convertToEntry(Entity entity);
}
