package com.interview.rtracks.manager;

import com.interview.rtracks.entity.Playlist;
import com.interview.rtracks.entry.PlaylistEntry;
import com.interview.rtracks.exception.ManagerException;
import org.springframework.transaction.annotation.Transactional;

public interface PlaylistManager extends BaseManager<Playlist, PlaylistEntry> {
    @Transactional(rollbackFor = Exception.class)
    void likePlaylist(Long playlistId) throws ManagerException;

    @Transactional(rollbackFor = Exception.class)
    void disLikePlaylist(Long playlistId) throws ManagerException;

    @Transactional(rollbackFor = Exception.class)
    void viewPlaylist(Long playlistId) throws ManagerException;

    @Transactional(rollbackFor = Exception.class)
    void addRelationship(Long id, Long tagId) throws ManagerException;

    @Transactional(rollbackFor = Exception.class)
    void removeRelationship(Long id, Long tagId) throws ManagerException;
}
