package com.interview.rtracks.manager;


import com.interview.rtracks.entity.BaseNodeEntity;
import com.interview.rtracks.entry.BaseEntry;
import com.interview.rtracks.exception.ManagerException;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface BaseManager<Entity extends BaseNodeEntity, Entry extends BaseEntry> {
    @Transactional(rollbackFor = Exception.class)
    Entry create(Entry entry) throws ManagerException;

    @Transactional(rollbackFor = Exception.class)
    Entry update(Long id, Entry entry) throws ManagerException;

    @Transactional(readOnly = true)
    Entry findById(Long id) throws ManagerException;

    @Transactional(rollbackFor = Exception.class)
    void delete(Long id) throws ManagerException;

    @Transactional(readOnly = true)
    List<Entry> findByTagNames(List<String> tagNames);
}
