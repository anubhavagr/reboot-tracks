package com.interview.rtracks.manager.impl;

import com.interview.rtracks.entity.Playlist;
import com.interview.rtracks.entity.Tag;
import com.interview.rtracks.entry.PlaylistEntry;
import com.interview.rtracks.entry.SearchEntry;
import com.interview.rtracks.entry.TagEntry;
import com.interview.rtracks.manager.PlaylistManager;
import com.interview.rtracks.manager.SearchManager;
import com.interview.rtracks.manager.TagManager;
import com.interview.rtracks.repository.PlaylistRepository;
import com.interview.rtracks.repository.TagRepository;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component("searchManager")
public class SearchManagerImpl implements SearchManager {

    private Mapper beanMapper;
    private TagManager tagManager;
    private PlaylistManager playlistManager;

    @Autowired
    public void setBeanMapper(Mapper beanMapper) {
        this.beanMapper = beanMapper;
    }

    @Autowired
    public void setTagManager(TagManager tagManager) {
        this.tagManager = tagManager;
    }

    @Autowired
    public void setPlaylistManager(PlaylistManager playlistManager) {
        this.playlistManager = playlistManager;
    }

    public SearchEntry search(String tagNames) {
        String[] splitTagNames = tagNames.split("\\+");
        List<TagEntry> tagEntries = tagManager.findByTagNames(Arrays.asList(splitTagNames));
        List<PlaylistEntry> playlistEntries = playlistManager.findByTagNames(Arrays.asList(splitTagNames));
        return new SearchEntry(tagEntries, playlistEntries);
    }
}
