package com.interview.rtracks.manager;

import com.interview.rtracks.entry.SearchEntry;

public interface SearchManager {
    SearchEntry search(String tags);
}
