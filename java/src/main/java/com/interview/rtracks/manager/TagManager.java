package com.interview.rtracks.manager;

import com.interview.rtracks.entity.Tag;
import com.interview.rtracks.entry.TagEntry;

import java.util.List;
import java.util.Set;

public interface TagManager extends BaseManager<Tag, TagEntry> {
}
