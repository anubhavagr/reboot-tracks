package com.interview.rtracks.manager.impl;

import com.interview.rtracks.entity.Playlist;
import com.interview.rtracks.entity.Tag;
import com.interview.rtracks.entry.PlaylistEntry;
import com.interview.rtracks.entry.TagWeightageIncrementEntry;
import com.interview.rtracks.exception.ManagerException;
import com.interview.rtracks.manager.PlaylistManager;
import com.interview.rtracks.repository.PlaylistRepository;
import com.interview.rtracks.repository.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Component;

@Component("playlistManager")
public class PlaylistManagerImpl extends BaseManagerImpl<Playlist, PlaylistEntry> implements PlaylistManager {
    private static final Long LIKE_INCREMENT_VALUE = 1L;
    private static final Long DISLIKE_INCREMENT_VALUE = -1L;
    private static final Long VIEW_INCREMENT_VALUE = 1L;
    private MessageChannel publishPlaylistLikedEventChannel;
    private MessageChannel publishPlaylistDislikedEventChannel;
    private MessageChannel publishPlaylistViewedEventChannel;
    private MessageChannel publishPlaylistDeletedEventChannel;
    private MessageChannel publishTagWeightageIncrementEventChannel;
    private TagRepository tagRepository;

    @Autowired
    @Qualifier("playlistRepository")
    public void setRepository(PlaylistRepository repository) {
        this.repository = repository;
    }

    @Autowired
    public void setPublishPlaylistLikedEventChannel(MessageChannel publishPlaylistLikedEventChannel) {
        this.publishPlaylistLikedEventChannel = publishPlaylistLikedEventChannel;
    }

    @Autowired
    public void setPublishPlaylistDislikedEventChannel(MessageChannel publishPlaylistDislikedEventChannel) {
        this.publishPlaylistDislikedEventChannel = publishPlaylistDislikedEventChannel;
    }

    @Autowired
    public void setPublishPlaylistViewedEventChannel(MessageChannel publishPlaylistViewedEventChannel) {
        this.publishPlaylistViewedEventChannel = publishPlaylistViewedEventChannel;
    }

    @Autowired
    public void setPublishPlaylistDeletedEventChannel(MessageChannel publishPlaylistDeletedEventChannel) {
        this.publishPlaylistDeletedEventChannel = publishPlaylistDeletedEventChannel;
    }

    @Autowired
    public void setPublishTagWeightageIncrementEventChannel(MessageChannel publishTagWeightageIncrementEventChannel) {
        this.publishTagWeightageIncrementEventChannel = publishTagWeightageIncrementEventChannel;
    }

    @Autowired
    public void setTagRepository(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    protected Playlist convertToEntity(PlaylistEntry playlistEntry) {
        return convertToEntity(playlistEntry, new Playlist());
    }

    protected Playlist convertToEntity(PlaylistEntry playlistEntry, Playlist playlist) {
        beanMapper.map(playlistEntry, playlist);
        return playlist;
    }

    protected PlaylistEntry convertToEntry(Playlist playlist) {
        return beanMapper.map(playlist, PlaylistEntry.class);
    }

    @Override
    public void likePlaylist(Long playlistId) {
        ((PlaylistRepository) repository).incrementLikes(playlistId, LIKE_INCREMENT_VALUE);
        Message<Playlist> message = getPlaylistMessage(playlistId);
        publishPlaylistLikedEventChannel.send(message);
    }

    private Message<Playlist> getPlaylistMessage(Long playlistId) {
        Playlist playlist = new Playlist();
        playlist.setId(playlistId);
        return MessageBuilder.withPayload(playlist).build();
    }

    @Override
    public void disLikePlaylist(Long playlistId) {
        ((PlaylistRepository) repository).incrementLikes(playlistId, DISLIKE_INCREMENT_VALUE);
        Message<Playlist> message = getPlaylistMessage(playlistId);
        publishPlaylistDislikedEventChannel.send(message);
    }

    @Override
    public void viewPlaylist(Long playlistId) {
        ((PlaylistRepository) repository).incrementViews(playlistId, VIEW_INCREMENT_VALUE);
        Message<Playlist> message = getPlaylistMessage(playlistId);
        publishPlaylistViewedEventChannel.send(message);
    }

    @Override
    public void delete(Long id) throws ManagerException {
        Playlist playlist = repository.findById(id, 1);
        super.delete(id);
        publishPlaylistDeletedEventChannel.send(MessageBuilder.withPayload(playlist).build());
    }

    @Override
    public void addRelationship(Long id, Long tagId) throws ManagerException {
        Playlist playlist = getPlaylist(id);
        Tag tag = getTag(tagId);
        boolean relationExist = isRelationExist(tag.getId(), playlist);
        if (!relationExist){
            ((PlaylistRepository) repository).createRelationship(id, tagId);
            publishTagWeightageIncrementEventChannel.send(MessageBuilder.withPayload(new TagWeightageIncrementEntry(tagId, playlist.getLikes() + playlist.getViews())).build());
        }
    }

    private Playlist getPlaylist(Long id) throws ManagerException {
        Playlist playlist = repository.findById(id, 1);
        if (playlist == null){
            throw new ManagerException("Invalid Playlist", 32L);
        }
        return playlist;
    }

    private Tag getTag(Long tagId) throws ManagerException {
        Tag tag = tagRepository.findById(tagId, 0);
        if (tag == null){
            throw new ManagerException("Invalid tag", 32L);
        }
        return tag;
    }

    private boolean isRelationExist(Long tagId, Playlist playlist) {
        boolean relationExist = false;
        if(playlist.getTags() != null){
            for (Tag ptag : playlist.getTags()) {
                if (ptag.getId().compareTo(tagId) == 0){
                    relationExist = true;
                    break;
                }
            }
        }
        return relationExist;
    }

    @Override
    public void removeRelationship(Long id, Long tagId) throws ManagerException {
        Playlist playlist = getPlaylist(id);
        Tag tag = getTag(tagId);
        boolean relationExist = isRelationExist(tag.getId(), playlist);
        if (relationExist){
            ((PlaylistRepository) repository).deleteRelationship(id, tagId);
            publishTagWeightageIncrementEventChannel.send(MessageBuilder.withPayload(new TagWeightageIncrementEntry(tagId, 0 -(playlist.getLikes() + playlist.getViews()))).build());
        }
    }
}
