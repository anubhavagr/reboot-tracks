package com.interview.rtracks.factory.impl;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.interview.rtracks.factory.ObjectMapperFactoryFactory;
import org.springframework.stereotype.Component;

@Component("objectMapperFactory")
public class ObjectMapperFactoryImpl implements ObjectMapperFactoryFactory {
    public ObjectMapper getMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        return mapper;
    }
}
