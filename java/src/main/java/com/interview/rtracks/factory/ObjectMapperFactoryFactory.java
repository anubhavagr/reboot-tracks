package com.interview.rtracks.factory;

import com.fasterxml.jackson.databind.ObjectMapper;

public interface ObjectMapperFactoryFactory {
    ObjectMapper getMapper();
}
