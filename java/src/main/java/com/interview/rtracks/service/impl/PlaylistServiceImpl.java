package com.interview.rtracks.service.impl;

import com.interview.rtracks.entity.Playlist;
import com.interview.rtracks.entry.PlaylistEntry;
import com.interview.rtracks.exception.ManagerException;
import com.interview.rtracks.manager.PlaylistManager;
import com.interview.rtracks.response.PlaylistResponse;
import com.interview.rtracks.response.Status;
import com.interview.rtracks.response.StatusType;
import com.interview.rtracks.service.PlaylistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

@Component("playlistService")
public class PlaylistServiceImpl extends BaseNodeServiceImpl<Playlist, PlaylistEntry, PlaylistResponse> implements PlaylistService {
    @Autowired
    @Qualifier("playlistManager")
    public void setManager(PlaylistManager manager) {
        this.manager = manager;
    }

    protected PlaylistResponse createResponse(List<PlaylistEntry> playlistEntries) {
        return new PlaylistResponse(playlistEntries);
    }

    @Override
    public PlaylistResponse likePlaylist(Long id) {
        PlaylistResponse response;
        try {
            ((PlaylistManager)manager).likePlaylist(id);
            response = createResponse(Collections.EMPTY_LIST);
            response.setStatus(new Status(2L, "Updated Successfully", StatusType.SUCCESS));
        } catch (ManagerException me) {
            response = createResponse(null);
            response.setStatus(new Status(me.getCode(), me.getMessage(), StatusType.ERROR));
        } catch (Exception e) {
            response = createResponse(null);
            response.setStatus(new Status(2L, "Error updating", StatusType.ERROR));
        }
        return response;
    }

    @Override
    public PlaylistResponse dislikePlaylist(Long id) {
        PlaylistResponse response;
        try {
            ((PlaylistManager)manager).disLikePlaylist(id);
            response = createResponse(Collections.EMPTY_LIST);
            response.setStatus(new Status(2L, "Updated Successfully", StatusType.SUCCESS));
        } catch (ManagerException me) {
            response = createResponse(null);
            response.setStatus(new Status(me.getCode(), me.getMessage(), StatusType.ERROR));
        } catch (Exception e) {
            response = createResponse(null);
            response.setStatus(new Status(2L, "Error updating", StatusType.ERROR));
        }
        return response;
    }

    @Override
    public PlaylistResponse viewPlaylist(Long id) {
        PlaylistResponse response;
        try {
            ((PlaylistManager)manager).viewPlaylist(id);
            response = createResponse(Collections.EMPTY_LIST);
            response.setStatus(new Status(2L, "Updated Successfully", StatusType.SUCCESS));
        } catch (ManagerException me) {
            response = createResponse(null);
            response.setStatus(new Status(me.getCode(), me.getMessage(), StatusType.ERROR));
        } catch (Exception e) {
            response = createResponse(null);
            response.setStatus(new Status(2L, "Error updating", StatusType.ERROR));
        }
        return response;
    }

    @Override
    public PlaylistResponse addTag(Long id, Long tagId) {
        PlaylistResponse response;
        try {
            ((PlaylistManager)manager).addRelationship(id, tagId);
            response = createResponse(Collections.EMPTY_LIST);
            response.setStatus(new Status(2L, "Updated Successfully", StatusType.SUCCESS));
        } catch (ManagerException me) {
            response = createResponse(null);
            response.setStatus(new Status(me.getCode(), me.getMessage(), StatusType.ERROR));
        } catch (Exception e) {
            response = createResponse(null);
            response.setStatus(new Status(2L, "Error updating", StatusType.ERROR));
        }
        return response;
    }

    @Override
    public PlaylistResponse deleteTag(Long id, Long tagId) {
        PlaylistResponse response;
        try {
            ((PlaylistManager)manager).removeRelationship(id, tagId);
            response = createResponse(Collections.EMPTY_LIST);
            response.setStatus(new Status(2L, "Updated Successfully", StatusType.SUCCESS));
        } catch (ManagerException me) {
            response = createResponse(null);
            response.setStatus(new Status(me.getCode(), me.getMessage(), StatusType.ERROR));
        } catch (Exception e) {
            response = createResponse(null);
            response.setStatus(new Status(2L, "Error updating", StatusType.ERROR));
        }
        return response;
    }
}
