package com.interview.rtracks.service;

import com.interview.rtracks.entity.BaseNodeEntity;
import com.interview.rtracks.entry.BaseEntry;
import com.interview.rtracks.response.BaseResponse;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

public interface BaseNodeService<Entity extends BaseNodeEntity, Entry extends BaseEntry, Response extends BaseResponse> {
    @Path("/")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response create(Entry entry);

    @Path("/{id}")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response update(Entry entry, @PathParam("id") Long id);

    @Path("/{id}")
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response findById(@PathParam("id") Long id);

    @Path("/{id}")
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response delete(@PathParam("id") Long id);
}
