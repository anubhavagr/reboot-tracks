package com.interview.rtracks.service;

import com.interview.rtracks.response.SearchResponse;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/search")
public interface SearchService {
    @GET
    @Path("/{tags}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    SearchResponse searchByTags(@PathParam("tags") String tags);
}
