package com.interview.rtracks.service.impl;

import com.interview.rtracks.entity.BaseNodeEntity;
import com.interview.rtracks.entry.BaseEntry;
import com.interview.rtracks.exception.ManagerException;
import com.interview.rtracks.manager.BaseManager;
import com.interview.rtracks.response.BaseResponse;
import com.interview.rtracks.response.Status;
import com.interview.rtracks.response.StatusType;
import com.interview.rtracks.service.BaseNodeService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class BaseNodeServiceImpl<Entity extends BaseNodeEntity, Entry extends BaseEntry, Response extends BaseResponse> implements BaseNodeService<Entity, Entry, Response> {
    protected BaseManager<Entity, Entry> manager;

    public void setManager(BaseManager manager) {
        this.manager = manager;
    }

    public Response create(Entry entry) {
        Response response;
        try {
            Entry savedEntry = manager.create(entry);
            response = createResponse(Collections.singletonList(savedEntry));
            response.setStatus(new Status(1L, "Created Successfully", StatusType.SUCCESS));
        } catch (ManagerException me) {
            response = createResponse(null);
            response.setStatus(new Status(me.getCode(), me.getMessage(), StatusType.ERROR));
        } catch (Exception e) {
            response = createResponse(null);
            response.setStatus(new Status(1L, "Error creating Tag", StatusType.ERROR));
        }
        return response;
    }

    public Response update(Entry entry, Long id) {
        Response response;
        try {
            Entry savedEntry = manager.update(id, entry);
            response = createResponse(Collections.singletonList(savedEntry));
            response.setStatus(new Status(2L, "Updated Successfully", StatusType.SUCCESS));
        } catch (ManagerException me) {
            response = createResponse(null);
            response.setStatus(new Status(me.getCode(), me.getMessage(), StatusType.ERROR));
        } catch (Exception e) {
            response = createResponse(null);
            response.setStatus(new Status(2L, "Error updating", StatusType.ERROR));
        }
        return response;
    }

    public Response findById(Long id) {
        Response response;
        try {
            Entry savedEntry = manager.findById(id);
            response = createResponse(Collections.singletonList(savedEntry));
            response.setStatus(new Status(3L, "Retrieved Successfully", StatusType.SUCCESS));
        } catch (ManagerException me) {
            response = createResponse(null);
            response.setStatus(new Status(me.getCode(), me.getMessage(), StatusType.ERROR));
        } catch (Exception e) {
            response = createResponse(null);
            response.setStatus(new Status(3L, "Error retrieving", StatusType.ERROR));
        }
        return response;
    }

    public Response delete(Long id) {
        Response response;
        try {
            manager.delete(id);
            response = createResponse(new ArrayList<Entry>());
            response.setStatus(new Status(4L, "Deleted Successfully", StatusType.SUCCESS));
        } catch (ManagerException me) {
            response = createResponse(null);
            response.setStatus(new Status(me.getCode(), me.getMessage(), StatusType.ERROR));
        } catch (Exception e) {
            response = createResponse(null);
            response.setStatus(new Status(4L, "Error deleting", StatusType.ERROR));
        }
        return response;
    }

    protected abstract Response createResponse(List<Entry> entries);
}
