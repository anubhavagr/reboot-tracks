package com.interview.rtracks.service;

import com.interview.rtracks.entity.Playlist;
import com.interview.rtracks.entry.PlaylistEntry;
import com.interview.rtracks.response.PlaylistResponse;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/playlist")
public interface PlaylistService extends BaseNodeService<Playlist, PlaylistEntry, PlaylistResponse> {
    @PUT
    @Path("/{id}/like")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    PlaylistResponse likePlaylist(@PathParam("id") Long id);

    @PUT
    @Path("/{id}/dislike")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    PlaylistResponse dislikePlaylist(@PathParam("id") Long id);

    @PUT
    @Path("/{id}/view")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    PlaylistResponse viewPlaylist(@PathParam("id") Long id);

    @POST
    @Path("/{id}/tag/{tagId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    PlaylistResponse addTag(@PathParam("id")Long id, @PathParam("tagId")Long tagId);

    @DELETE
    @Path("/{id}/tag/{tagId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    PlaylistResponse deleteTag(@PathParam("id")Long id, @PathParam("tagId")Long tagId);
}
