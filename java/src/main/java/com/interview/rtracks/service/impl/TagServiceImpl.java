package com.interview.rtracks.service.impl;

import com.interview.rtracks.entity.Tag;
import com.interview.rtracks.entry.TagEntry;
import com.interview.rtracks.manager.TagManager;
import com.interview.rtracks.response.TagResponse;
import com.interview.rtracks.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("tagService")
public class TagServiceImpl extends BaseNodeServiceImpl<Tag, TagEntry, TagResponse> implements TagService {
    @Autowired
    @Qualifier("tagManager")
    public void setManager(TagManager manager) {
        this.manager = manager;
    }

    protected TagResponse createResponse(List<TagEntry> tagEntries) {
        return new TagResponse(tagEntries);
    }
}
