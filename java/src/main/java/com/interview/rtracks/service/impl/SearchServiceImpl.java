package com.interview.rtracks.service.impl;

import com.interview.rtracks.entry.SearchEntry;
import com.interview.rtracks.manager.SearchManager;
import com.interview.rtracks.response.SearchResponse;
import com.interview.rtracks.response.Status;
import com.interview.rtracks.response.StatusType;
import com.interview.rtracks.service.SearchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.PathParam;

@Service("searchService")
public class SearchServiceImpl implements SearchService {
    private static final Logger logger = LoggerFactory.getLogger(SearchServiceImpl.class);
    private SearchManager manager;

    @Autowired
    public void setManager(SearchManager manager) {
        this.manager = manager;
    }

    public SearchResponse searchByTags(String tags) {
        SearchResponse response = new SearchResponse();
        try {
            SearchEntry searchEntry = manager.search(tags);
            response.setSearch(searchEntry);
            response.setStatus(new Status(6L, "Success", StatusType.SUCCESS));

        } catch (Exception e) {
            logger.error("Error Searching", e);
            response.setStatus(new Status(5L, "Error Searching", StatusType.ERROR));
        }
        return response;
    }
}
