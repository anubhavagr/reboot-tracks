package com.interview.rtracks.service;


import com.interview.rtracks.entity.Tag;
import com.interview.rtracks.entry.TagEntry;
import com.interview.rtracks.response.TagResponse;

import javax.ws.rs.Path;

@Path("/tag")
public interface TagService extends BaseNodeService<Tag, TagEntry, TagResponse> {
}
