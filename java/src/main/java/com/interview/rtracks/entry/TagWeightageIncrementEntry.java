package com.interview.rtracks.entry;

public class TagWeightageIncrementEntry {
    private Long tagId;
    private Long weightageIncrement;

    public Long getTagId() {
        return tagId;
    }

    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }

    public Long getWeightageIncrement() {
        return weightageIncrement;
    }

    public void setWeightageIncrement(Long weightageIncrement) {
        this.weightageIncrement = weightageIncrement;
    }

    public TagWeightageIncrementEntry() {
    }

    public TagWeightageIncrementEntry(Long tagId, Long weightageIncrement) {
        this.tagId = tagId;
        this.weightageIncrement = weightageIncrement;
    }
}
