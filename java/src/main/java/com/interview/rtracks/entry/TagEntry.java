package com.interview.rtracks.entry;

import java.util.List;

public class TagEntry extends BaseEntry{
    private String name;
    private Long weightage;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getWeightage() {
        return weightage;
    }

    public void setWeightage(Long weightage) {
        this.weightage = weightage;
    }
}
