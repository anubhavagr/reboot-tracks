package com.interview.rtracks.entry;

import java.io.Serializable;

public abstract class BaseEntry implements Serializable {
    protected Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
