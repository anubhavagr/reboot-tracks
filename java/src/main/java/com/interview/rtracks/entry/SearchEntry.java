package com.interview.rtracks.entry;

import com.interview.rtracks.entity.Playlist;

import java.util.List;

public class SearchEntry {
    private List<TagEntry> tags;
    private List<PlaylistEntry> playlists;

    public SearchEntry() {
    }

    public SearchEntry(List<TagEntry> tags, List<PlaylistEntry> playlists) {
        this.tags = tags;
        this.playlists = playlists;
    }

    public List<TagEntry> getTags() {
        return tags;
    }

    public void setTags(List<TagEntry> tags) {
        this.tags = tags;
    }

    public List<PlaylistEntry> getPlaylists() {
        return playlists;
    }

    public void setPlaylists(List<PlaylistEntry> playlists) {
        this.playlists = playlists;
    }
}
