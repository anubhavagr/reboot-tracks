package com.interview.rtracks.listener.impl;

import com.interview.rtracks.entity.Playlist;
import com.interview.rtracks.entity.Tag;
import com.interview.rtracks.entry.TagWeightageIncrementEntry;
import com.interview.rtracks.listener.EventListener;
import com.interview.rtracks.repository.PlaylistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("playlistLikedEventListener")
public class PlaylistLikedEventListener implements EventListener<Playlist> {
    private PlaylistRepository playlistRepository;
    private MessageChannel publishTagWeightageIncrementEventChannel;

    @Autowired
    public void setPlaylistRepository(PlaylistRepository playlistRepository) {
        this.playlistRepository = playlistRepository;
    }

    @Autowired
    public void setPublishTagWeightageIncrementEventChannel(MessageChannel publishTagWeightageIncrementEventChannel) {
        this.publishTagWeightageIncrementEventChannel = publishTagWeightageIncrementEventChannel;
    }

    @Override
    public void processMessage(Message<Playlist> message) {
        Playlist playlist = message.getPayload();
        Playlist dbPlaylist = playlistRepository.findById(playlist.getId(), 1);
        List<Tag> tags = dbPlaylist.getTags();
        if (tags != null){
            for (Tag tag:tags) {
                TagWeightageIncrementEntry tagWeightageIncrementEntry = new TagWeightageIncrementEntry(tag.getId(), 1L);
                Message<TagWeightageIncrementEntry> tagMessage = MessageBuilder.withPayload(tagWeightageIncrementEntry).build();
                publishTagWeightageIncrementEventChannel.send(tagMessage);
            }
        }
    }
}
