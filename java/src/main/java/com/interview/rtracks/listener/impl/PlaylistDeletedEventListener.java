package com.interview.rtracks.listener.impl;

import com.interview.rtracks.entity.Playlist;
import com.interview.rtracks.entity.Tag;
import com.interview.rtracks.entry.TagWeightageIncrementEntry;
import com.interview.rtracks.listener.EventListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("playlistDeletedEventListener")
public class PlaylistDeletedEventListener implements EventListener<Playlist> {
    private MessageChannel publishTagWeightageIncrementEventChannel;

    @Autowired
    public void setPublishTagWeightageIncrementEventChannel(MessageChannel publishTagWeightageIncrementEventChannel) {
        this.publishTagWeightageIncrementEventChannel = publishTagWeightageIncrementEventChannel;
    }

    @Override
    public void processMessage(Message<Playlist> message) {
        Playlist playlist = message.getPayload();
        Long weightageLoss = 0 - (playlist.getLikes() + playlist.getViews());
        List<Tag> tags = playlist.getTags();
        if (tags != null){
            for (Tag tag:tags) {
                TagWeightageIncrementEntry tagWeightageIncrementEntry = new TagWeightageIncrementEntry(tag.getId(), weightageLoss);
                Message<TagWeightageIncrementEntry> tagMessage = MessageBuilder.withPayload(tagWeightageIncrementEntry).build();
                publishTagWeightageIncrementEventChannel.send(tagMessage);
            }
        }
    }
}
