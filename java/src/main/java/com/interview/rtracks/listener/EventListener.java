package com.interview.rtracks.listener;

public interface EventListener<T> {
    void processMessage(org.springframework.messaging.Message<T> message);
}
