package com.interview.rtracks.listener.impl;

import com.interview.rtracks.entry.TagWeightageIncrementEntry;
import com.interview.rtracks.listener.EventListener;
import com.interview.rtracks.repository.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

@Component("tagWeightageIncrementEventListener")
public class TagWeightageIncrementEventListener implements EventListener<TagWeightageIncrementEntry> {
    private TagRepository tagRepository;

    @Autowired
    public void setTagRepository(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public void processMessage(Message<TagWeightageIncrementEntry> message) {
        TagWeightageIncrementEntry tagWeightageIncrementEntry = message.getPayload();
        tagRepository.incrementWeightage(tagWeightageIncrementEntry.getTagId(), tagWeightageIncrementEntry.getWeightageIncrement());
    }
}
