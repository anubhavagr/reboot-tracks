package com.interview.rtracks.repository;

import com.interview.rtracks.entity.BaseNodeEntity;

import java.util.List;

public interface NodeRepository<Entity extends BaseNodeEntity> {
    Entity save(Entity t, int depth);
    Entity findById(Long id, int depth);
    List<Entity> findByTagNames(List<String> tagNames);
    void delete(Entity entity);
}
