package com.interview.rtracks.repository;

import com.interview.rtracks.entity.Playlist;
import com.interview.rtracks.entity.Tag;

public interface TagRepository extends NodeRepository<Tag> {
    Tag incrementWeightage(Long playlistId, Long incrementalWeightageValue);
}
