package com.interview.rtracks.repository;

import com.interview.rtracks.entity.Playlist;

import java.util.List;

public interface PlaylistRepository extends NodeRepository<Playlist> {
    List<Playlist> findByTagNames(List<String> tagNames);
    void incrementLikes(Long playlistId, Long incrementalLikesValue );
    void incrementViews(Long playlistId, Long incrementalViewsValue);
    boolean relationshipExist(Long playlistId, Long tagId);
    void createRelationship(Long playlistId, Long tagId);
    void deleteRelationship(Long playlistId, Long tagId);
}
