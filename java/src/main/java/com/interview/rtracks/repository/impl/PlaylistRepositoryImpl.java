package com.interview.rtracks.repository.impl;


import com.interview.rtracks.config.AppConstants;
import com.interview.rtracks.entity.Playlist;
import com.interview.rtracks.repository.PlaylistRepository;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringJoiner;

@Component("playlistRepository")
public class PlaylistRepositoryImpl extends NodeRepositoryImpl<Playlist> implements PlaylistRepository {

    public List<Playlist> findByTagNames(List<String> tagNames) {
        String query = getFindByTagsQuery(tagNames);
        Iterable<Playlist> dbPlaylists = neo4jOperations.queryForObjects(Playlist.class, query, new HashMap<String, Object>());
        ArrayList<Playlist> playlists = new ArrayList<>();
        if (dbPlaylists != null) {
            for (Playlist gpPlaylist : dbPlaylists) {
                playlists.add(gpPlaylist);
            }
        }
        return playlists;
    }

    private String getFindByTagsQuery(List<String> tagNames) {
        StringJoiner matchQueryBuilder = new StringJoiner(",");
        StringJoiner whereQueryBuilder = new StringJoiner(" and ");

        for (int index = 1; index <= tagNames.size(); index++) {
            matchQueryBuilder.add("(p:" + AppConstants.PLAYLIST_LABEL + ")-[:" + AppConstants.RELATIONSHIP_LABEL + "]->(t" + index + ":" + AppConstants.TAG_LABEL + ")");
            whereQueryBuilder.add("t" + index + ".name=\'" + tagNames.get(index - 1) + "\'");
        }
        return "MATCH " + matchQueryBuilder.toString()
                + " WHERE " + whereQueryBuilder.toString()
                + " RETURN p"
                + " ORDER BY p.likes DESC";
    }

    @Override
    public void incrementLikes(Long playlistId, Long incrementalLikesValue) {
        String updateIncrementalLikesQuery = updateIncrementalQuery(playlistId, "likes", incrementalLikesValue);
        neo4jOperations.query(updateIncrementalLikesQuery, new HashMap<String, Object>());
        neo4jOperations.clear();
    }

    private String updateIncrementalQuery(Long playlistId, String field, Long incrementalValue) {
        return "MATCH (p:" + AppConstants.PLAYLIST_LABEL + ")" +
                " WHERE ID(p)=" + playlistId +
                " SET p." + field + " = p." + field + " + " + incrementalValue + ";";
    }

    @Override
    public void incrementViews(Long playlistId, Long incrementalViewsValue) {
        String updateIncrementalViewsQuery = updateIncrementalQuery(playlistId, "views", incrementalViewsValue);
        neo4jOperations.query(updateIncrementalViewsQuery, new HashMap<String, Object>());
        neo4jOperations.clear();
    }

    @Override
    public boolean relationshipExist(Long playlistId, Long tagId) {
        String query = relationshipExistQuery(playlistId, tagId);
        Playlist playlist = neo4jOperations.queryForObject(Playlist.class, query, new HashMap<String, Object>());
        neo4jOperations.clear();
        return playlist != null;
    }

    private String relationshipExistQuery(Long playlistId, Long tagId) {
        return "MATCH (p:" + AppConstants.PLAYLIST_LABEL + ")-[r:" + AppConstants.RELATIONSHIP_LABEL + "]-(t:" + AppConstants.TAG_LABEL + ")" +
                " WHERE ID(p)=" + playlistId + " and ID(t)=" + tagId +
                " RETURN p";
    }

    @Override
    public void createRelationship(Long playlistId, Long tagId) {
        String query = createRelationshipQuery(playlistId, tagId);
        neo4jOperations.query(query, new HashMap<String, Object>());
        neo4jOperations.clear();
    }

    private String createRelationshipQuery(Long playlistId, Long tagId) {
        return "MATCH (t:" + AppConstants.TAG_LABEL + "),(p:" + AppConstants.PLAYLIST_LABEL + ")\n" +
                "WHERE ID(p)=" + playlistId + " and ID(t)=" + tagId + "\n" +
                "MERGE (p)-[:" + AppConstants.RELATIONSHIP_LABEL + "]->(t)";
    }

    @Override
    public void deleteRelationship(Long playlistId, Long tagId) {
        String query = deleteRelationshipQuery(playlistId, tagId);
        neo4jOperations.query(query, new HashMap<String, Object>());
        neo4jOperations.clear();

    }

    private String deleteRelationshipQuery(Long playlistId, Long tagId) {
        return "MATCH (p:" + AppConstants.PLAYLIST_LABEL + ")-[r:" + AppConstants.RELATIONSHIP_LABEL + "]->(t:" + AppConstants.TAG_LABEL + ")\n" +
                "WHERE ID(p)=" + playlistId + " and ID(t)=" + tagId + "\n" +
                "DELETE r;";
    }
}
