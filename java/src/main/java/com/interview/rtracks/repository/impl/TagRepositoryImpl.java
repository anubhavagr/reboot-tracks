package com.interview.rtracks.repository.impl;


import com.interview.rtracks.config.AppConstants;
import com.interview.rtracks.entity.Tag;
import com.interview.rtracks.repository.TagRepository;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringJoiner;

@Component("tagRepository")
public class TagRepositoryImpl extends NodeRepositoryImpl<Tag> implements TagRepository {

    @Override
    public List<Tag> findByTagNames(List<String> tagNames) {
        String query = getFindByRelatedTagsQuery(tagNames);
        Iterable<Tag> tags = neo4jOperations.queryForObjects(Tag.class, query, new HashMap<String, Object>());
        neo4jOperations.clear();
        List<Tag> Tags = getTags(tags);
        return Tags;
    }

    @Override
    public Tag incrementWeightage(Long playlistId, Long incrementalWeightageValue) {
        String updateIncrementalLikesQuery = updateIncrementalQuery(playlistId, "weightage", incrementalWeightageValue);
        Tag tag = neo4jOperations.queryForObject(Tag.class, updateIncrementalLikesQuery, new HashMap<String, Object>());
        neo4jOperations.clear();
        return tag;
    }

    private String updateIncrementalQuery(Long playlistId, String field, Long incrementalValue) {
        return "MATCH (t:" + AppConstants.TAG_LABEL + ")" +
                " WHERE ID(t)=" + playlistId +
                " SET t." + field + " = t." + field + " + " + incrementalValue +
                " RETURN t;";
    }

    private String getFindByRelatedTagsQuery(List<String> tagNames) {
        StringJoiner matchQueryBuilder = new StringJoiner(",");
        StringJoiner whereQueryBuilder = new StringJoiner(" and ");

        for (int index = 1; index <= tagNames.size(); index++) {
            matchQueryBuilder.add("(p:" + AppConstants.PLAYLIST_LABEL + ")-[:" + AppConstants.RELATIONSHIP_LABEL + "]->(t" + index + ":" + AppConstants.TAG_LABEL + ")");
            whereQueryBuilder.add("t" + index + ".name=\'" + tagNames.get(index - 1) + "\'");
        }

        String relatedTag = "relatedTag";
        matchQueryBuilder.add("(p:" + AppConstants.PLAYLIST_LABEL + ")-[:" + AppConstants.RELATIONSHIP_LABEL + "]->(" + relatedTag + ":" + AppConstants.TAG_LABEL + ")");


        return "MATCH " + matchQueryBuilder.toString()
                + " WHERE " + whereQueryBuilder.toString()
                + " RETURN relatedTag"
                + " ORDER BY relatedTag.weightage DESC";
    }

    private List<Tag> getTags(Iterable<Tag> tags) {
        List<Tag> Tags = new ArrayList<Tag>();
        if (tags != null) {
            for (Tag Tag : tags) {
                Tags.add(Tag);
            }
        }
        return Tags;
    }
}
