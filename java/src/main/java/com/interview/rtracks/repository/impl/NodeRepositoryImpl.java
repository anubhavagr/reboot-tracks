package com.interview.rtracks.repository.impl;

import com.interview.rtracks.entity.BaseNodeEntity;
import com.interview.rtracks.repository.NodeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.neo4j.template.Neo4jOperations;

import java.lang.reflect.ParameterizedType;

public abstract class NodeRepositoryImpl<Entity extends BaseNodeEntity> implements NodeRepository<Entity> {
    protected final Class<Entity> entityClass;
    protected Neo4jOperations neo4jOperations;

    public NodeRepositoryImpl() {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        this.entityClass = (Class<Entity>) genericSuperclass.getActualTypeArguments()[0];
    }

    public Neo4jOperations getNeo4jOperations() {
        return neo4jOperations;
    }

    @Autowired
    public void setNeo4jOperations(Neo4jOperations neo4jOperations) {
        this.neo4jOperations = neo4jOperations;
    }

    public Entity save(Entity t, int depth) {
        return neo4jOperations.save(t, depth);
    }

    public Entity findById(Long id, int depth) {
        return neo4jOperations.load(entityClass, id, depth);
    }

    public void delete(Entity entity) {
        neo4jOperations.delete(entity);
    }
}
