package com.interview.rtracks.entity;

import org.neo4j.ogm.annotation.Property;
import org.neo4j.ogm.annotation.Relationship;

import java.util.List;

@org.neo4j.ogm.annotation.NodeEntity(label = "playlist")
public class Playlist extends BaseNodeEntity {
    @Property(name="name")
    private String name;

    @Property(name="url")
    private String url;

    @Property(name="likes")
    private Long likes;

    @Property(name="views")
    private Long views;

    @Relationship(type="TAGS", direction=Relationship.OUTGOING)
    private List<Tag> tags;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getLikes() {
        return likes;
    }

    public void setLikes(Long likes) {
        this.likes = likes;
    }

    public Long getViews() {
        return views;
    }

    public void setViews(Long views) {
        this.views = views;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }
}
