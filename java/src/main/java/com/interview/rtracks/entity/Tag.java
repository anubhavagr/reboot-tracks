package com.interview.rtracks.entity;

import org.neo4j.ogm.annotation.Property;

@org.neo4j.ogm.annotation.NodeEntity(label = "tag")
public class Tag extends BaseNodeEntity {
    @Property(name="name")
    private String name;

    @Property(name="weightage")
    private Long weightage;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getWeightage() {
        return weightage;
    }

    public void setWeightage(Long weightage) {
        this.weightage = weightage;
    }
}
