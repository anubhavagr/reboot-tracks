package com.interview.rtracks.entity;

import org.neo4j.ogm.annotation.GraphId;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class BaseNodeEntity {
    @GraphId
    protected Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
